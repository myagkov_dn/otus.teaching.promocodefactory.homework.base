﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Dtos;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public async Task<Guid> CreateEmployeesAsync(EmployeeWriteDto e)
        {
            var idEmployee = await _employeeRepository.CreateAsync(new Employee 
            {
                FirstName = e.FirstName, 
                LastName = e.LastName, 
                Email = e.Email, 
                Roles = e.Roles, 
                AppliedPromocodesCount = e.AppliedPromocodesCount 
            });

            return idEmployee.Id;
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpDelete()]
        public async Task<Guid> DeleteEmployeesAsync(Guid g)
        {
            var idEmployee = await _employeeRepository.DeleteAsync(g);

            return idEmployee;
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPut("Employee/{id}")]
        public async Task<Guid> UpdateEmployeesAsync(Guid id,EmployeeWriteDto e)
        {

            var idEmployee = await _employeeRepository.UpdateAsync(new Employee
            {
                Id = id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Email = e.Email,
                Roles = e.Roles,
                AppliedPromocodesCount = e.AppliedPromocodesCount
            });

            return idEmployee.Id;
        }
    }
}